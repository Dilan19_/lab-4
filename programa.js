class Encarcelado{
    
    //Atributos
    _palabra;
    _letra;

    //Método constructor se ejecuta cuando se crea un objeto
    constructor(valor){
        this._palabra=valor;
    }

    //Funciones

    set palabra(valor){
        this._palabra=valor;
    }

    set letra(valor){
        this._letra=valor;
    }


    iniciarJuego(){
        //verificar el tamaño de la palabra
        let a=this._palabra.length;
        let boton;
        let i;
        let formulario=document.getElementById("frmTablero");
        

        for(i=0;i<a;i++){
            //crear botones
            boton=document.createElement("input");
            boton.setAttribute("type", "button");
            boton.setAttribute("class", "boton");
            formulario.appendChild(boton);
        } 
        
    }


    verficaJuego() 
    {
        let cont = 0;
            for(let i=0;i<this._palabra.length;i++){
                if(this._letra == this._palabra.charAt(i)){
                    cont++;
                }
            }
            if(cont==0){
                alert("¡Muy Mal! Intenta de nuevo");
                return this._letra; 
            }
            else{
                alert("¡Has acertado! Sigue Jugando ");
                return this._letra;
            }    
    }
}

let miJuego=new Encarcelado("universidad");
miJuego.iniciarJuego();

